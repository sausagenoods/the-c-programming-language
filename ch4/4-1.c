#include <stdio.h>

/* The function strrindex(s,t) returns the rightmost occurrence of
	t in s, or -1 if there's none */

int strrindex(char *s, char t)
{
	int occ = 0, i = 0;
	
	for (; *s != '\0'; *s++) {
		i++;
		if (*s == t)
			occ = i;
	}
	
	if (occ) {return occ;}
	else {return -1;}
}

int main()
{
	int pos;
	char *muh_string = "All moi loife. lOl cars on bikes.";
	pos = strrindex(muh_string, 'o');
	printf("Char position: %d\n", pos);
	return 0;
}
