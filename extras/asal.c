#include <stdio.h>
#include <math.h>

/* This is basically me raging over a faulty algorithm. This was one of the math olympiad questions we tried to solve in math class. */

/* bu algoritma girilen bir dogal sayinin asal olup olmadigini su sekilde buluyor:
   1: Dogal sayiyi gir.
   2: Girilen dogal sayinin karekokunden kucuk asal sayilara bolunup bolunmedigini kontrol et.
   3: Hicbirine bolunmuyorsa ekrana "ASAL" yaz, en az birine bolunuyorsa asal degil yaz.
*/

int main() {

    int asal_sayilar[25] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97}; // 100'e kadar olan asal sayilarin listesi.

    int girilen_sayi, i;
    printf("Bir sayi giriniz: ");
    scanf("%d", &girilen_sayi); // girilen dogal sayi.

    int asal = 1; // sayi asal ise 1 degerini alir, degilse 0.
    int deneme_sayisi = 0; 
    for(i = 0; i < 25; i++) {   // asal sayilar listesindeki her eleman icin
        if(sqrt((double)girilen_sayi) > asal_sayilar[i]) {   // eleman, girilen sayinin karekokunden kucuk ise
            deneme_sayisi++;
            if(girilen_sayi%asal_sayilar[i]==0) { // eger girilen sayi herhangi bir elemana bolunuyorsa
                asal = 0; // sayi asal degildir.
                // break; // bu algoritmanin kucuk bir kusuru var o da bolunebilen sayi bulsa bile digerlerini test etmeye devam etmesi.
                // daha iyi bir algoritma ilk bolunebilir sayiyi bulduktan sonra duran bir algoritma olurdu.
            }
        }
    }

    if(asal)
        printf("ASAL!\n");
    else
        printf("ASAL DEGIL!\n");

    printf("kontrol edilen sayi sayisi: %d\n", deneme_sayisi);

    return 0;
}
