#include <windows.h>
#include <stdio.h>
#include <string.h>

__declspec(naked) int assembly()
{
    int i = 4;
    asm
    (
        "mov %0, %%eax\n\t"
        : : "a"(i)
    );
}

int main()
{
    unsigned char buffer[15] = { 0 };

    HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, TRUE, GetCurrentProcessId());
    GetLastError();
    WriteProcessMemory(hProc, &buffer, &assembly, 7, NULL);
    GetLastError();

    for(int i = 0; i < 15; i++)
        printf("%02X ", buffer[i]);

    return 0;
}
