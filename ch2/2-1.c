#include <stdio.h>
#include <math.h>
#include <limits.h>

/* A program to determine the ranges of char, short, int and long variables, 
 * both signed and unsigned, by printing appropriate values from standart 
 * headers and by direct computations. */

int main() 
{
	/* Using direct computation:
	 * get bits using ~0
	 * shift 1 bit to remove the sign bit >> 1
	*/
	
	// printf("%d\n", CHAR_BIT);
	printf("Signed char:	[%d, %d]\n", -(char) ((unsigned char) ~0 >> 1) -1,
			(char) ((unsigned char) ~0 >> 1));
	// printf("Sc: %d\t%d\n", SCHAR_MIN, SCHAR_MAX); 
	printf("Unsigned char: 	[%u, %u]\n", 0, (unsigned char)~0);
	
	/* Using symbolic constants from limits.h */	
	printf("Short: 		[%d, %d]\n", SHRT_MIN, SHRT_MAX); 
	printf("Unsigned short: [%d, %d]\n", 0, USHRT_MAX); 
	printf("Int: 		[%d, %d]\n", INT_MIN, INT_MAX);	
	printf("Unsigned Int: 	[%u, %u]\n", 0, UINT_MAX); 
	printf("Long: 		[%ld, %ld]\n", LONG_MIN, LONG_MAX);
	printf("Unsigned long: 	[%lu, %lu]\n", 0, ULONG_MAX); 

	return 0;
}
