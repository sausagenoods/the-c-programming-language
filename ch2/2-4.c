#include <stdio.h>

/* an alternate version of squeeze(s1, s2) that deletes each 
 * character in s1 that matches any character in the string s2. */

void squeeze(char s1[], char s2[]);

int main() 
{
	char text[] = "Armillaria can be a destructive forest pathogen. "
		"It causes \"white rot\" root disease of forests, which "
		"distinguishes it from Tricholoma, a mycorrhizal "
		"(non-parasitic) genus. Because Armillaria is a "
		"facultative saprophyte, it also feeds on dead plant "
		"material, allowing it to kill its host, unlike "
		"parasites that must moderate their growth to avoid host death.";
	
	char rm[] = "AaBbCc";

	puts(text);
	puts("---");
	squeeze(text, rm);
	puts(text);

	return 0;
}

void squeeze(char s1[], char s2[]) 
{
	int i, j, k, l;

	l = 1;

	for (i = j = 0; s1[i] != '\0'; i++) {
		l = 0;	
		for (k = 0; s2[k] != '\0'; k++) {
			if (s1[i] == s2[k]) {l = 1;}
		}
		if (!l) { s1[j++] = s1[i];}
	}
	s1[j] = '\0';
}
