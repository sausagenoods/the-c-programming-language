#include <stdio.h>

/* Write a loop equivalent to the for loop from ch1 without using && or ||. */

#define MAXLINE 1000

int main() 
{
	char line[MAXLINE];
	int c, i;
	i = 0;

	while ((c = getchar()) != '\n') {
		if (i > MAXLINE-1)
			break;
		
		if (c == EOF)
			break;
		
		line[i] = c;
		i++;
	}
	
	line[i+1] = '\0';	
	puts(line);

	return 0;
}
