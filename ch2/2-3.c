#include <stdio.h>
#include <ctype.h>
#include <math.h>

/* Write the function htoi(s), which converts a string of hexadecimal digits 
 * (including an optional 0x or 0X) into its equivalent integer value */

unsigned long htoi(char hex[]);

int main(int argc, char *argv[]) 
{
	char *test[6] = {
		"kernals",
		"0x23",
		"abcdef",
		"01011",
		"DEADBEEF",
		"0x2019"
	};

	for (int i = 0; i < 6; i++)
		printf("%10s -> %10lu\n", test[i], htoi(test[i]));

	return 0;
}

unsigned long htoi(char hex[])
{
	int i, c;
	unsigned long dec = 0;
	i = 0;

	// ignore the first 2 chars if prefixed with 0x
	if (tolower(hex[1]) == 'x') i = 2; 
	
	while ((c = tolower(hex[i])) != '\0') {
		if (c >= '0' && c <= '9')
			dec = 16 * dec + c - '0';	
		else if (c >= 'a' && c <= 'f')
			dec = 16 * dec + c - 'a' + 10;
		else 
			return 0;
			
		++i;
	}
	return dec;
}
