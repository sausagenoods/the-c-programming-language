#include <unistd.h>
#include <fcntl.h>

/* for benchmarking */
#include <stdio.h>
#include <sys/time.h>

int main (int argc, char *argv[]) {    
    char buf[512];
    int fd, n;
    struct timeval start, end;
    double elapsed;

    gettimeofday(&start, 0);

    if (argc < 2) {
        while ((n = read(0, buf, 512)) > 0)
            write(1, buf, n);
    } else {
        fd = open(argv[1], O_RDONLY, 0);
        if (fd == -1) {
            write(2, "Failed to return a file descriptor\n", 35);
            return -1;
        }
        
        while ((n = read(fd, buf, 512)) > 0)
            write(1, buf, n);
    }
    close(n);

    gettimeofday(&end, 0);
    elapsed = end.tv_sec + end.tv_usec / 1e6 -
                start.tv_sec - start.tv_usec / 1e6;
    
    printf("Time elapsed: %f", elapsed); 

    return 0;
}
