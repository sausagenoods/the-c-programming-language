#include <stdio.h>

/* a program to copy its input to its output, replacing each string of one or more blanks by a single blank. */

int main() {
    int c, blanks;
    blanks = 0;
    
    while ((c=getchar()) != EOF) {
        if (c == ' ') {
            if (blanks == 0)
                putchar(c);
			
            blanks = 1;
        }
        else {
            putchar(c);
            blanks = 0;
        }
    }

    return 0;
}
