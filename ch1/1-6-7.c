#include <stdio.h>

int main() {
    
    /* print the value of EOF */
    printf("the value of EOF is %d", EOF);

    int c;

    while((c=getchar())!=EOF) {
        putchar(c);
    }
   
    printf("the value of the statement above is %d", c);

    return 0;
}
