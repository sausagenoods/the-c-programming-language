#include <stdio.h>

/* A program to fold long input lines into two or more shorter lines
 * after the last non-blank character that occurs before the n-th
 * column of input. */

#define MAXLINE 100
#define MAXLEN 20

int getlines(char line[]);
void fold(char s1[], char s2[]);

int main()
{
	int len;
	char line[MAXLINE];
	char folded[MAXLINE];
	
	while ((len = getlines(line)) > 0) {
		if (len > MAXLEN) {
			fold(line, folded);
			printf("\e[0;31m%s\e[0m", folded);
		} else {
			printf("\e[0;31m%s\e[0m", line); 
		}
	}

	return 0;
}

int getlines(char line[])
{
	int c, i;
	for (i = 0; i < MAXLINE - 1 
		&& (c = getchar()) != EOF && c != '\n'; ++i) {
	       line[i] = c;
       	}
	
	if (c == '\n') {
		line[i] = c;
		++i;
	}
	
	line[i] = '\0';
	return i;
}

void fold(char s1[], char s2[])
{
	int i, blank;

	#define is_blank(c) (c == ' ' || c == '\n' || c == '\t')
	
	for (i = 0; s1[i] != '\0'; i++) {
		if (is_blank(s1[i])) { blank = i; }

		if (i > 0 && !(i % MAXLEN)) {
			if (is_blank(s1[i-1])) { s2[i-1] = '\n'; }
			else { s2[blank] = '\n'; }	      
		}
			
		s2[i] = s1[i];
	}
	s2[i++] = '\0';
}



