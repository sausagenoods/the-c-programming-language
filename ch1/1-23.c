#include <stdio.h>

/* A program to remove all comments from a C program. */

#define SOURCE "1-23.c"
// #define TARGET "23r.c"

FILE *fp;

int main() 
{
	char line[128];
	char tline[128];

	fp = fopen(SOURCE, "r"); 
	
	int i, out;
	out = 0;

	while(fgets(line, sizeof(line), fp) != NULL)
	{
		for (i = 0; line[i] != '\0'; i++) {
			if (line[i] == '/' && line[i+1] == '/') { out = 1; }
			if (line[i] == '/' && line[i+1] == '*') { out = 1; }
			if (line[i] == '*' && line[i+1] == '/') { out = 0; }
			if(!out) { tline[i] = line[i]; }
		}
		out = 0;
		tline[i++] = '\0';
		fputs(tline, stdout);
	}

	fclose(fp);

	return 0;
}
