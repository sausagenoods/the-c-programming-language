#include <stdio.h>

#define MAXLINE 1000
#define TABSTOP 4

int getlines(FILE *fp, char *line);
int autoident(char *line);

int level;

int main(int argc, char *argv[])
{
        FILE *fp;

        /* do some checks */
        if (argc != 2) {
                puts("Usage: ./fix file.c");
                return 1;
        } else {
                fp = fopen(argv[1], "r");
        }
        
        if (!fp) { perror("fopen"); return 1;}

        char line[MAXLINE];
        level = 0;
        int len;
        while ((len = getlines(fp, line)) > 0) 
                autoident(line);
               
        return 0;

}

int autoident(char *line) 
{
    int i;

    for (i = 0; line[i] == ' '; i++) // skip blanks
        ;

    for (int c = 0; level != 0 && c <= level*TABSTOP; c++)
        putchar(' ');

    for (; line[i] != '\0'; i++) {
        putchar(line[i]);

        if (line[i] == '{') 
            level++;
           
        if (line[i] == '}') 
            level--;
    }
}


int getlines(FILE *fp, char line[])
{
        int c, i;
        for (i = 0; i < MAXLINE - 1
                && (c = fgetc(fp)) != EOF && c != '\n'; ++i) {
                line[i] = c;
        }

        if (c == '\n') {
                line[i] = c;
                ++i;
        }

        line[i] = '\0';
        return i;
}


