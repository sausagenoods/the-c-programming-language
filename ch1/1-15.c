#include <stdio.h>

/* rewrite the temperature conversion program to use a function for conversion */

int to_celcius(int fahr) {
        int celcius;
        celcius = 5 * (fahr-32) / 9;
        return celcius;
}

int main() {
        int fahr;
        int lower, upper, step;
        lower = 0;
        upper = 300;
        step = 20;

        for(fahr = lower; fahr <= upper; fahr += step) {
                printf("%d\t%d\n", fahr, to_celcius(fahr));
        }
}
        
