#include <stdio.h>

/* write a program to print the corresponding celsius to fahrenheit table.
 * modify it to print a heading above the table. */

int main(void) {
    float celsius, fahr;
    int upper, step;
	 
    upper = 300;
    step = 20;
    celsius = 0; /* the lower limit */

    printf("Celsius-Fahrenheit Table\n");

    while(celsius <= upper) {
        fahr = celsius * (9.0/5.0) + 32.0;
        /* celsius is to be printed at least 3 chars wide with no decimal point.
         * fahr is to be printed 6 chars wide. */
        printf("%3.0f %6.0f\n", celsius, fahr); 
        celsius = celsius  + step;
    }
}


