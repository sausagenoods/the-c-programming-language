#include <stdio.h>

/* it's bad practice to bury "magic numbers" like 300 and 20; 
   they convey little information to someone who might have to read the program later. */

#define UPPER 300
#define LOWER 0
#define STEP 20

/* modify the table to print in reverse order, that is, from 300 degrees to 0. */

int main() {
    int fahr;

    printf("Fahrenheit-Celsius Table\n");
    for(fahr=LOWER; fahr <= UPPER; fahr += STEP) {
        printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr-32.0));
    }
}
