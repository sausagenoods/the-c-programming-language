#include <stdio.h>
#define MAXLINE 20

/* Write a function that reverses a character string. */

int getlines(char line[], int maxline);
void reverse(char line[], int size);

int main() 
{
	char line[MAXLINE];
	int c;

	while ((c = getlines(line, MAXLINE)) > 0) {
		reverse(line, c);
		printf(line);
	}

	return 0;
}

int getlines(char line[], int maxline)
{
	int i, c;

	for (i = 0; i < maxline-1 && (c=getchar()) != EOF && c != '\n'; i++) 
		line[i] = c;
		
	if (c = '\n') {
		line[i] = c;
		i++;
	}

	line[i] = '\0';

	return i;
}

void reverse(char line[], int size)
{
	int i;
	for (i = 0; i < size/2; i++) { // divide the size by 2 otherwise it swaps every element twice
		int temp = line[i]; // first val of the element
		line[i] = line[size -2 -i]; // '\n\0'
		line[size -2 -i] = temp;
	}	
}
