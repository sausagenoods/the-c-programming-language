#include <stdio.h>

/* A program to print all input lines that are longer than 80 characters. */

int main(void)
{
	char *line;
	size_t len = 0;
	ssize_t nread;

	while ((nread = getline(&line, &len, stdin)) != -1) { 
		if (nread >= 80)
			printf(line);
	}

	return 0;
}


