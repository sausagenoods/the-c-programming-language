#include <stdio.h>

/* a program to count blanks, tabs, and newlines. */7

int main() {
    int c, blanks, tabs, newline;

    blanks = tabs = newline = 0;

    while ((c=getchar()) != EOF) {
        if (c == ' ')
            blanks++;
        else if (c == '\t')
            tabs++;
        else if (c == '\n')
            newline++;
    }

    printf("blanks = %d, tabs = %d, newlines = %d\n", blanks, tabs, newline);

    return 0;
}
